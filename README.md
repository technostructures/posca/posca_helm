# Posca Helm chart

## Install

```bash
helm repo add technostructures https://technostructures.gitlab.io/helm-charts
helm install my-release technostructures/posca
```

## Uninstall

```bash
helm delete my-release
```

## Parameters

| Name                  | Description                   | Value                     |
|:--------------------- | ----------------------------- | ------------------------- |
| `posca.matrix.domain` | Default Matrix domain         | `posca.pm`                |
| `posca.matrix.url`    | Default Matrix homeserver URL | `https://matrix.posca.pm` |
